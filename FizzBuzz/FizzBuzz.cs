﻿using System;
using System.Collections.Generic;

namespace FizzBuzzProject {
    public class FizzBuzz {
        public static List<string> result = new List<string>();
        public static void Main(string[] args) {
            for (int i = 1; i < 100; i++) {
                result.Add(DoFizzOrBuzz(i));
            }
            
            foreach (string s in result) Console.WriteLine(s);
        }

        public static string DoFizzOrBuzz(int n) {
            if (n % 3 == 0 && n % 5 == 0) return "FizzBuzz";
            else if (n % 3 == 0) return "Fizz";
            else if (n % 5 == 0) return "Buzz";
            else return $"{n}";
        }
    }
}
